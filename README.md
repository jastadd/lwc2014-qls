JastAdd-Ql
==========

A questionary language according to LWC2014.

Build
=====

Use ant to build the project.

     $ cd your/path/to/jastaddql
     $ ant
     $ ant jar

Usage
=====

          $ cd your/path/to/jastaddql
     JAR: $ java -jar jastadd-ql.jar [filename] [options]
     NIX: $ java -cp .:ant-bin/ gui.controller.ApplicationRunner [filename] [options]
     WIN: $ java -cp ".;ant-bin/" gui.controller.ApplicationRunner [filename] [options]
     IDE: From a custom run configuration

Arguments
=========

    [filename]: The file that specifies the Ql-Form, e.g. testfiles/interface/BigBinary.ql
          [-w]: Optional argument to display warnings
          [-d]: Optional argument to display debugging info
