form f "Birthday" {
  q0: "What's your name ?" string:"Sheldon"
  q1: "When do you have birthday this year ?" date:"01.07.2015"
  q2: "What's today's date ?" date
  if(q1 == q2) {
    q3:"Happy Birthday" string(q0 + ", old pal")
  } else {
    if(q1 < q2) {
      q3: "You have already celebrated your birthday this year" string("Happy belated birthday " + q0 + ", old pal")
    } else {
      q3: "You have not celebrated your birthday this year" string("Be patient, " + q0)
    }
  }
}