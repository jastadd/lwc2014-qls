form f "Test form" {
  q1: "Addition = Yes Subtraktion=No" boolean
  q2: "The 2. label?" money
  q3: "The 3. label?" money
  if(q1) {
    q4: "q2 + q3" decimal(q2 + q3)
  } else {
    q5: "q2 - q3" decimal(q2 - q3)
  }
}