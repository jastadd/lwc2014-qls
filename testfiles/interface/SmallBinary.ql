form binary "Guess a number:" {
  x_0_6: "Is the number between 0 and 6" boolean
  if (x_0_6) {
    x_0_3: "Is the number between 0 and 3" boolean
    if (x_0_3) {
      x_0_2: "Is the number between 0 and 2" boolean
      if (x_0_2) {
        x_1: "Is the number 1?" boolean
        if(x_1) {
          answer_0: "The answer is" integer(1)
        } else {
          answer_1: "The answer is" integer(0)
        }
      } else {
        answer_0_2: "The number is" string(">2")
      }
    } else {
      answer_0_6: "The number is" string(">3")
    }
  } else {
    answer_0_6: "The number is" string(">6")
  }
}
