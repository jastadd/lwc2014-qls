form f "Check visibilty" {
  q1: "The first label?" boolean
  if(q1) {
    q2: "The second label?" boolean
    if(q2) {
      q3: "The third label?" boolean
      q4: "The fourth label?" boolean
    }
  }
  q5: "The 5th label?" boolean
  if(q5) {
    q6: "The 6th label?" money
    q7: "The 7th label?" money
    q8: "The 8th label?" money
    q9: "The 9th label?" money
    q10: "The 10th label?" date
    q11: "The 11th label?" money
    q12: "The 12th label?" money
    q13: "The 13th label?" money
    q14: "The 14th label?" money
    q15: "The 15th label?" money
    q16: "The 16th label?" money
    q17: "The 17th label?" money
    q18: "The 18th label?" money
    q19: "The 19th label?" money
  }
  q50: "Numeral" decimal(1 + "3")
}