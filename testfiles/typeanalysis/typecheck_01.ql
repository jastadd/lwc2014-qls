form f "typecheck_01.ql" {
  q1: "A question label?" boolean
  q2: "A question label?" boolean
  q3: "A question label?" boolean(q1 + q2)
  q4: "A question label?" boolean(q1 - q2)
  q5: "A question label?" boolean(q1 * q2)
  q6: "A question label?" boolean(q1 / q2)
  q7: "A question label?" boolean(q1 < q2)
  q8: "A question label?" boolean(q1 > q2)
  q9: "A question label?" boolean(q1 <= q2)
  qa: "A question label?" boolean(q1 >= q2)
  qb: "A question label?" boolean(q1 == q2)
  qc: "A question label?" boolean(q1 != q2)
  qd: "A question label?" boolean(!q1)
}