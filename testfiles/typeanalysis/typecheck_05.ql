form f "typecheck_05.ql" {
  q1: "A question label?" boolean(true)
  q2: "A question label?" boolean(false)
  q3: "A question label?" integer(3)
  q4: "A question label?" decimal(3.1415)
  q5: "A question label?" string("string")
  q6: "A question label?" date("23.03.1967")
  if(true) {
    i1: "A question label?" boolean
  }
  if(false) {
    i2: "A question label?" boolean
  }
  if(3) {
    i3: "A question label?" boolean
  }
  if(3.1415) {
    i4: "A question label?" boolean
  }
  if("string") {
    i5: "A question label?" boolean
  }
  if("23.03.1967") {
    i6: "A question label?" boolean
  }
}