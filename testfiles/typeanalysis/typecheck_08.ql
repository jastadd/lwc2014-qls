form f "typecheck_08.ql" {
  a1: "A question label?" string
  if(false) {
    a1: "A question label?" boolean
    if(false) {
      a1: "A question label?" date
      if(false) {
        a1: "A question label?" integer
        if(false) {
          a1: "A question label?" money
          if(false) {
            a1: "A question label?" decimal
            if(false) {
              a1: "A question label?" string
            }
          }
        }
      }
    }
  }
}