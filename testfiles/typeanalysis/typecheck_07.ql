form f "typecheck_01.ql" {
  a1: "A question label?" string
  a2: "A question label?" boolean
  a3: "A question label?" string
  a4: "A question label?" date
  a5: "A question label?" integer
  a6: "A question label?" money
  a7: "A question label?" decimal

  b1: "A question label?" string
  b2: "A question label?" boolean
  b3: "A question label?" string
  b4: "A question label?" date
  b5: "A question label?" integer
  b6: "A question label?" money
  b7: "A question label?" decimal

  qaa: "A question label?" boolean(a1 == b2)
  qab: "A question label?" boolean(a2 == b3)
  qac: "A question label?" boolean(a3 == b4)
  qad: "A question label?" boolean(a4 == b5)
  qae: "A question label?" boolean(a5 == b6)
  qaf: "A question label?" boolean(a6 == b7)
  qag: "A question label?" boolean(a7 == b1)

  qba: "A question label?" boolean(a1 != b2)
  qbb: "A question label?" boolean(a2 != b3)
  qbc: "A question label?" boolean(a3 != b4)
  qbd: "A question label?" boolean(a4 != b5)
  qbe: "A question label?" boolean(a5 != b6)
  qbf: "A question label?" boolean(a6 != b7)
  qbg: "A question label?" boolean(a7 != b1)
}