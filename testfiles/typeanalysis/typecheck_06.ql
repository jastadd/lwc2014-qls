form f "typecheck_06.ql" {
  q1: "A question label?" string
  q2: "A question label?" boolean
  q3: "A question label?" string
  q4: "A question label?" date
  q5: "A question label?" integer
  q6: "A question label?" money
  q7: "A question label?" decimal
  qa: "A question label?" string(q1 + q2)
  qb: "A question label?" string(q1 + q3)
  qc: "A question label?" string(q1 + q4)
  qd: "A question label?" string(q1 + q5)
  qe: "A question label?" string(q1 + q6)
  qf: "A question label?" string(q1 + q7)
}