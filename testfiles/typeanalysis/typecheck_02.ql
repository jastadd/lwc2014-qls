form f "typecheck_02.ql" {
  i1: "A question label?" integer
  i2: "A question label?" integer
  i3: "A question label?" integer(i1 + i2)
  i4: "A question label?" integer(i1 - i2)
  i5: "A question label?" integer(i1 * i2)
  i6: "A question label?" integer(i1 / i2)
  i7: "A question label?" boolean(i1 < i2)
  i8: "A question label?" boolean(i1 > i2)
  i9: "A question label?" boolean(i1 <= i2)
  ia: "A question label?" boolean(i1 >= i2)
  ib: "A question label?" boolean(i1 == i2)
  ic: "A question label?" boolean(i1 != i2)
  id: "A question label?" boolean(!i1)

  d1: "A question label?" decimal
  d2: "A question label?" decimal
  d3: "A question label?" decimal(d1 + d2)
  d4: "A question label?" decimal(d1 - d2)
  d5: "A question label?" decimal(d1 * d2)
  d6: "A question label?" decimal(d1 / d2)
  d7: "A question label?" boolean(d1 < d2)
  d8: "A question label?" boolean(d1 > d2)
  d9: "A question label?" boolean(d1 <= d2)
  da: "A question label?" boolean(d1 >= d2)
  db: "A question label?" boolean(d1 == d2)
  dc: "A question label?" boolean(d1 != d2)
  dd: "A question label?" boolean(!d1)

  m1: "A question label?" money
  m2: "A question label?" money
  m3: "A question label?" money(m1 + m2)
  m4: "A question label?" money(m1 - m2)
  m5: "A question label?" money(m1 * m2)
  m6: "A question label?" money(m1 / m2)
  m7: "A question label?" boolean(m1 < m2)
  m8: "A question label?" boolean(m1 > m2)
  m9: "A question label?" boolean(m1 <= m2)
  ma: "A question label?" boolean(m1 >= m2)
  mb: "A question label?" boolean(m1 == m2)
  mc: "A question label?" boolean(m1 != m2)
  md: "A question label?" boolean(!m1)

}