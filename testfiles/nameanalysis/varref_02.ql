form f "varref_02.ql" {
  q1: "Q1 ?" boolean
  if(q1) {
    q2: "Q2 ?" boolean
  } else {
    q3: "Q3 ?" boolean
    if(q3) {
      a1: "A1" boolean(q2) // q2 should be found
      a2: "A2" boolean(q4) // q4 shouldn't be found
    }
  }
  q4: "Q4 ?" boolean(q2) // q2 should be found
  q5: "Q5 ?" boolean(q3) // q3 should be found
  q6: "Q6 ?" boolean(q7) // q7 shouldn't be found
  q7: "Q7 ?" boolean
  q8: "Q8" boolean(q8)   // not OK: self-reference in definition; q1 is used before its definition
}