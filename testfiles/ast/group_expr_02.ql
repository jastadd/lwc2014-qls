form f "group_expr_02" {
  q1: "The first label?" boolean
  if(q1) {
    q2: "The second label?" money
  }
}