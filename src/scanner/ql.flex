package lang.ast; // The generated scanner will belong to the package lang.ast

import lang.ast.LangParser.Terminals; // The terminals are implicitly defined in the parser
import lang.ast.LangParser.SyntaxError;

%%

// define the signature for the generated scanner
%public
%final
%class LangScanner
%extends beaver.Scanner

// the interface between the scanner and the parser is the nextToken() method
%type beaver.Symbol 
%function nextToken 

%unicode
// store line and column information in the tokens
%line
%column

// this code will be inlined in the body of the generated scanner class
%{
  StringBuffer strbuf = new StringBuffer(128);
  int strlit_start_line, strlit_start_column;
  
  private beaver.Symbol sym(short id) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }
  
  private beaver.Symbol sym(short id, String value, int start_line, int start_column, int len) {
    return new beaver.Symbol(id, start_line, start_column, len, value);
  }
  
  private String str() { return yytext(); }

%}

// macros
LineTerminator = \n|\r|\r\n
InputCharacter = [^\r\n]
StringCharacter = [^\r\n\"\\]

WhiteSpace = [ ] | \t | \f | {LineTerminator}

Comment = {TraditionalComment} | {EndOfLineComment}

TraditionalComment = "/*" [^*] ~"*/" | "/*" "*"+ "/" | "/*" "*"+ [^/*] ~"*/"
EndOfLineComment = "//" {InputCharacter}* {LineTerminator}?

NAME = [a-zA-Z0-9_]+
IntegerNumeral = [0-9]+
DecimalNumeral = [0-9]+ "." [0-9]+

%state STRING

%%

<YYINITIAL> {
  // discard whitespace and comments
  {WhiteSpace}  { }
  {Comment} { }

  // token definitions
  "form"            { return sym(Terminals.FORM); }
  "boolean"         { return sym(Terminals.BOOLEAN); }
  "string"          { return sym(Terminals.STRING); }
  "integer"         { return sym(Terminals.INTEGER); }
  "date"            { return sym(Terminals.DATE); }
  "decimal"         { return sym(Terminals.DECIMAL); }
  "money"           { return sym(Terminals.MONEY); }
  "if"              { return sym(Terminals.IF); }
  "else"            { return sym(Terminals.ELSE); }
  "&&"              { return sym(Terminals.ANDAND); }
  "||"              { return sym(Terminals.OROR); }
  "!"               { return sym(Terminals.NOT); }
  ">"               { return sym(Terminals.GT); }
  "<"               { return sym(Terminals.LT); }
  "=="              { return sym(Terminals.EQEQ); }
  "<="              { return sym(Terminals.LTEQ); }
  ">="              { return sym(Terminals.GTEQ); }
  "!="              { return sym(Terminals.NOTEQ); }
  "+"               { return sym(Terminals.PLUS); }
  "-"               { return sym(Terminals.MINUS); }
  "/"               { return sym(Terminals.DIV); }
  "*"               { return sym(Terminals.MULT); }
  ":"               { return sym(Terminals.COLON); }
  "("               { return sym(Terminals.LPAREN); }
  ")"               { return sym(Terminals.RPAREN); }
  "{"               { return sym(Terminals.LBRACE); }
  "}"               { return sym(Terminals.RBRACE); }
  "true"            { return sym(Terminals.BOOLEAN_LITERAL); }
  "false"           { return sym(Terminals.BOOLEAN_LITERAL); }
  {IntegerNumeral}  { return sym(Terminals.NUMERAL_LITERAL); }
  {DecimalNumeral}  { return sym(Terminals.NUMERAL_LITERAL); }
  {NAME}            { return sym(Terminals.NAME); }
  <<EOF>>           { return sym(Terminals.EOF); }
  
  \" {
    yybegin(STRING);
    /* remember start position of string literal so we can */
    /* set its position correctly in the end */
    strlit_start_line = yyline+1;
    strlit_start_column = yycolumn+1;
    strbuf.setLength(0);
  }
  
  /* error fallback */
  [^]        { throw new SyntaxError("Illegal character <"+yytext()+">"); }

}

<STRING> {
  \"       { yybegin(YYINITIAL);
             return sym(Terminals.STRING_LITERAL, strbuf.toString(), strlit_start_line, strlit_start_column, strbuf.length()+2); 
           }
  {StringCharacter}+             { strbuf.append(str()); }

  "\\\""                         { strbuf.append( '\"' ); }
  "\\'"                          { strbuf.append( '\'' ); }
  "\\\\"                         { strbuf.append( '\\' ); }
  \\.                            { throw new SyntaxError("illegal escape sequence \""+str()+"\""); }
  {LineTerminator}               { throw new SyntaxError("unterminated string at end of line"); }
}

