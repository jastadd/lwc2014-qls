package tests;

import org.junit.Test;

@SuppressWarnings("javadoc")
public class ParseTests extends AbstractTestSuite {
	public ParseTests() {
		super("testfiles/parser");// where test input files are
	}

	@Test
	public void simpleForm() {
    testValidSyntax("form_01.ql");
	}
	@Test
	public void wrongType() {
    testSyntaxError("form_02.ql");
	}
	@Test
	public void noFormName() {
    testSyntaxError("form_03.ql");
	}
	@Test
	public void noFormLabel() {
    testSyntaxError("form_04.ql");
	}
}
