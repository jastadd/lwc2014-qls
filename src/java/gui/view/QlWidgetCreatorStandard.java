package gui.view;

import lang.ast.*;

public class QlWidgetCreatorStandard extends QlWidgetCreator {
  
  public QlWidget widgetFactory(ASTNode modelNode) {
    
      QlWidget qlWidget = null;

      if(modelNode instanceof Form) {
        Form formNode = (Form)modelNode;
        qlWidget = new QlWidgetFormStandard(formNode);
      } else if(modelNode instanceof Field) {
        Field fieldNode = (Field)modelNode;
        if(fieldNode instanceof ComputedField) {
          qlWidget = new QlWidgetComputedStandard(fieldNode);
        } else {
          Type type = fieldNode.getType();
          if (type instanceof BoolType) {
            qlWidget = new QlWidgetBooleanStandard(fieldNode);
          } else if (type instanceof NumeralType) {
            qlWidget = new QlWidgetNumeralStandard(fieldNode);
          } else if (type instanceof DateType) {
            qlWidget = new QlWidgetDateStandard(fieldNode);
          } else {
            qlWidget = new QlWidgetStringStandard(fieldNode);
          }
        }
      }
      return qlWidget;
  }

}
