package gui.view;

import lang.ast.BooleanValue;
import lang.ast.Field;
import lang.ast.ModelValue;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * User: joerg
 * Created: 07.03.15 08:32
 */
public class QlWidgetBooleanStandard extends QlWidget implements ActionListener {

  private JRadioButton yesRadioButton = new JRadioButton();
  private JRadioButton noRadioButton = new JRadioButton();
  private ButtonGroup radioGroup = new ButtonGroup();

  public QlWidgetBooleanStandard(Field modelNode) {
    super(modelNode);
    
    String labelText = modelNode.getLabel();
    ModelValue value = modelNode.getModelValue();

    JLabel label = new JLabel();
    label.setForeground(new Color(254,254,254));
    label.setText(labelText);
    label.setBorder(null);
    this.add(label);

    JPanel radioPanel = new JPanel();
    SpringLayout springLayout = new SpringLayout();
    radioPanel.setLayout(springLayout);
    radioPanel.setOpaque(false);
    radioPanel.setBackground(Color.DARK_GRAY);
    radioPanel.setBorder(BorderFactory.createCompoundBorder(
            null,
            BorderFactory.createEmptyBorder(2, 2, 2, 2)
        )
    );

    radioGroup.add(yesRadioButton);
    radioGroup.add(noRadioButton);

    yesRadioButton.setText("Yes");
    yesRadioButton.setOpaque(false);
    yesRadioButton.setBackground(Color.DARK_GRAY);
    yesRadioButton.setForeground(new Color(254, 254, 254));
    yesRadioButton.setBorder(new EmptyBorder(0, 0, 0, 5));
    yesRadioButton.setFocusPainted(false);
    yesRadioButton.addActionListener(this);
    radioPanel.add(yesRadioButton);

    noRadioButton.setText("No");
    noRadioButton.setOpaque(false);
    noRadioButton.setBackground(Color.DARK_GRAY);
    noRadioButton.setForeground(new Color(254, 254, 254));
    noRadioButton.setBorder(new EmptyBorder(0, 0, 0, 0));
    noRadioButton.setFocusPainted(false);
    noRadioButton.addActionListener(this);
    springLayout.putConstraint(SpringLayout.WEST, noRadioButton, 5, SpringLayout.EAST, yesRadioButton);
    radioPanel.add(noRadioButton);

    this.setValue(value);

    springLayout.putConstraint(SpringLayout.SOUTH, radioPanel, 1, SpringLayout.SOUTH, yesRadioButton);
    this.add(radioPanel);

    GridLayout gridlayout = new GridLayout(2, 1);
    gridlayout.setVgap(0);
    gridlayout.setHgap(0);

    this.setLayout(gridlayout);
    this.setOpaque(false);
    this.setAlignmentX(Component.LEFT_ALIGNMENT);
    this.setBorder(new EmptyBorder(0, 0, 10, 0));
    this.setVisible(false);
  }

  @Override
  public void setValue(ModelValue value) {
    if(value.isBooleanValue()) {
      BooleanValue booleanValue = (BooleanValue) value;
      if(booleanValue.getViewValue() == true) {
        yesRadioButton.setSelected(true);
      } else {
        noRadioButton.setSelected(true);
      }
    }
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if(yesRadioButton.isSelected()) {
      updateModelNode(new BooleanValue(true));
    } else if(noRadioButton.isSelected()) {
      updateModelNode(new BooleanValue(false));
    }
  }
}
