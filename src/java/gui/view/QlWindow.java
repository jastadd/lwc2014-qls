package gui.view;

import gui.controller.ApplicationRunner;
import lang.ast.Program;

import javax.swing.*;
import java.awt.*;


@SuppressWarnings("serial")
public class QlWindow extends JFrame {
  
  private JPanel qlPanel = ApplicationRunner.getInstance(JPanel.class);

	public QlWindow() {
		super();

    try {

      qlPanel.setLayout(new BoxLayout(qlPanel, BoxLayout.Y_AXIS));
      qlPanel.setBackground(Color.DARK_GRAY);
      qlPanel.setBorder(BorderFactory.createCompoundBorder(
          null,
          BorderFactory.createEmptyBorder(10, 10, 10, 10)));

      Program program = ApplicationRunner.getProgram();

      ApplicationRunner.updateModel();
      ApplicationRunner.updateView();
      ApplicationRunner.showStaticErrors();
      ApplicationRunner.dumpTree();

      JScrollPane qlScrollpane = ApplicationRunner.getInstance(JScrollPane.class);
      qlScrollpane.setViewportView(qlPanel);
      qlScrollpane.setPreferredSize(this.getMinimumSize());
      qlScrollpane.setAutoscrolls(true);
      qlScrollpane.getVerticalScrollBar().setUnitIncrement(20);
      qlScrollpane.setBorder(null);

      this.setWindowTitle(program.fileName);
      this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      this.setMinimumSize(new Dimension(500, 500));
      this.add(qlScrollpane);
  		this.pack();
  		this.setLocationRelativeTo(null);
  		this.setVisible(true);

    } catch (Exception e) {
      e.printStackTrace();
    }
	}

	public void setWindowTitle(String additionalTitle) {
		String title = "QL Form";
		if (additionalTitle != null && !additionalTitle.isEmpty()) {
			title += " - " + additionalTitle;
		}
		this.setTitle(title);
	}
  
  public void updateView() {
//    JPanel qlPanel = ApplicationRunner.getInstance(JPanel.class);
    Rectangle visible = qlPanel.getVisibleRect();
    Rectangle bounds = qlPanel.getBounds();
    visible.y = bounds.height;
    qlPanel.scrollRectToVisible(visible);
  }
}
