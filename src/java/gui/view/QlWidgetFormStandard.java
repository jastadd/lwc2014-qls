package gui.view;

import lang.ast.Form;
import lang.ast.StringValue;
import lang.ast.ModelValue;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Created by joerg on 11.03.15.
 */
public class QlWidgetFormStandard extends QlWidget {

  private JLabel labelTitle;

  public QlWidgetFormStandard(Form modelNode) {
    super(modelNode);

    String title = modelNode.getLabel();

    labelTitle = new JLabel(title);
    labelTitle.setForeground(new Color(254, 254, 254));
    labelTitle.setBorder(new EmptyBorder(0, 0, 5, 0));
    Font font = labelTitle.getFont();
    Font boldFont = new Font(font.getFontName(), Font.BOLD, font.getSize() + 2);
    labelTitle.setFont(boldFont);
    this.add(labelTitle);
    
    
    GridLayout gridlayout = new GridLayout(1, 1);
    gridlayout.setVgap(0);
    gridlayout.setHgap(0);

    this.setLayout(gridlayout);
    this.setOpaque(false);
    this.setAlignmentX(Component.LEFT_ALIGNMENT);
    this.setBorder(new EmptyBorder(0, 0, 10, 0));
    this.setValue(new StringValue(title));

  }

  @Override
  public void setValue(ModelValue value) {
    if(value.isStringValue()) {
      String title = ((StringValue) value).getViewValue();
      if(title.isEmpty()) {
        this.setVisible(false);
      } else {
        labelTitle.setText(title);
        this.setVisible(true);
      }
    }
  }
}
