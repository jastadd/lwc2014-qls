package gui.view;

import lang.ast.Field;
import lang.ast.NumeralValue;
import lang.ast.ModelValue;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * User: joerg
 * Created: 07.03.15 08:32
 */
public class QlWidgetNumeralStandard extends QlWidget implements PropertyChangeListener {

  private JFormattedTextField formattedTextField;

  public QlWidgetNumeralStandard(Field modelNode) {
    super(modelNode);

    String labelText = modelNode.getLabel();
    ModelValue value = modelNode.getModelValue();

    JLabel label = new JLabel();
    label.setForeground(new Color(254,254,254));
    label.setText(labelText);
//    label.setBorder(null);
    this.add(label);

    NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.getDefault());
    DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
    decimalFormat.setMaximumFractionDigits(10);
    decimalFormat.setGroupingUsed(false);

    formattedTextField = new JFormattedTextField(decimalFormat);
    formattedTextField.setText("");
    formattedTextField.addPropertyChangeListener("value", this);
    formattedTextField.setBorder(BorderFactory.createCompoundBorder(
        null,
        BorderFactory.createEmptyBorder(2, 2, 2, 2)));
    this.add(formattedTextField);

    this.setValue(value);

    GridLayout gridlayout = new GridLayout(2, 1);
    gridlayout.setVgap(0);
    gridlayout.setHgap(0);

    this.setLayout(gridlayout);
    this.setOpaque(false);
    this.setAlignmentX(Component.LEFT_ALIGNMENT);
    this.setBorder(new EmptyBorder(0, 0, 10, 0));
    this.setVisible(false);

    // Propagate input if focus is lost
    formattedTextField.addFocusListener(new FocusAdapter() {
      @Override
      public void focusGained(FocusEvent e) {
        formattedTextField.setBackground(Color.YELLOW.darker());
      }

      @Override
      public void focusLost(FocusEvent e) {
        formattedTextField.setBackground(Color.WHITE);
      }
    });
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    JFormattedTextField formattedTextField = (JFormattedTextField) evt.getSource();
    Object formattedTextFieldValue = formattedTextField.getValue();
    if(formattedTextFieldValue != null) {
      formattedTextField.setValue(((Number) formattedTextFieldValue).doubleValue());
      updateModelNode(new NumeralValue(formattedTextField.getValue().toString()));
    }
  }

  @Override
  public void setValue(ModelValue value) {
    if (value.isNumeralValue()) {
      NumeralValue numeralValue = (NumeralValue) value;
      formattedTextField.setText(numeralValue.toString());
    } else {
      formattedTextField.setText(ModelValue.getUndefinedValue().toString());
    }
  }
}
