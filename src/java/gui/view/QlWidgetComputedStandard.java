package gui.view;

import lang.ast.Field;
import lang.ast.ModelValue;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Created by joerg on 11.03.15.
 */
public class QlWidgetComputedStandard extends QlWidget {

  private JLabel labelComputed;

  public QlWidgetComputedStandard(Field modelNode) {
    super(modelNode);

    String labelText = modelNode.getLabel();

    JLabel label = new JLabel();
    label.setText(labelText);
    label.setForeground(new Color(254,254,254));
    label.setBorder(new EmptyBorder(0, 0, 2, 0));
//    label.setBorder(null);
    this.add(label);

    labelComputed = new JLabel();
    labelComputed.setText("");
    labelComputed.setOpaque(true);
//    labelComputed.setBackground(new Color(254,254,254));
//    labelComputed.setForeground(Color.BLACK);
    labelComputed.setBackground(Color.ORANGE.darker());
    labelComputed.setForeground(new Color(254, 254, 254));
    labelComputed.setBorder(BorderFactory.createCompoundBorder(
            null,
            BorderFactory.createEmptyBorder(2, 2, 2, 2)
        )
    );

    this.add(labelComputed);

    GridLayout gridlayout = new GridLayout(2, 1);
    gridlayout.setVgap(0);
    gridlayout.setHgap(0);

    this.setLayout(gridlayout);
    this.setOpaque(false);
    this.setAlignmentX(Component.LEFT_ALIGNMENT);
    this.setBorder(new EmptyBorder(0, 0, 10, 0));
    this.setVisible(false);

  }

  @Override
  public void setValue(ModelValue value) {
    labelComputed.setText(value.toString());
  }
}
