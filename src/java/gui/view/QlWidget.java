package gui.view;

import gui.controller.ApplicationRunner;
import lang.ast.ASTNode;
import lang.ast.Field;
import lang.ast.ModelValue;

import javax.swing.*;
import java.awt.*;


/**
 * User: joerg
 * Created: 07.03.15 08:39
 */
public abstract class QlWidget extends JPanel {
  
  protected lang.ast.ASTNode modelNode = null;

  public QlWidget(ASTNode modelNode) {
    this.modelNode = modelNode;
  }

  public abstract void setValue(ModelValue value);

  public void updateModelNode(ModelValue value) {

    if(this.modelNode instanceof Field) {
      Field field = (Field)this.modelNode;
      System.out.println("updateModelNode[" + field.getVarDecl().getName() + "]: " + value.toString());
    }
    this.modelNode.setModelValue(value);

    ApplicationRunner.updateModel();
    ApplicationRunner.updateView();
    ApplicationRunner.showRuntimeProblems();
    ApplicationRunner.dumpTree();
  }

  @Override
  public Dimension getMaximumSize() {
    Dimension preferredSize = getPreferredSize();
    int preferredSizeHeight = (int)preferredSize.getHeight();
    Dimension maxsize = new Dimension(Integer.MAX_VALUE, preferredSizeHeight);
    return maxsize;
  }

  @Override
  public String toString() {
    String widgetInfo = getClass().getSimpleName();
    if(this.modelNode != null && this.modelNode instanceof Field) {
      Field f  = (Field)this.modelNode;
      widgetInfo += " [model.name: " + f.getLabel() + ", model.value: " + f.getModelValue() + ", model.visible: " + f.isVisible() + ", view.visible: " + this.isVisible() + "]";
    }
    return widgetInfo;
  }
}
