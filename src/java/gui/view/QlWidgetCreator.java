package gui.view;

import lang.ast.ASTNode;

public abstract class QlWidgetCreator {
  public abstract QlWidget widgetFactory(ASTNode modelNode);
}