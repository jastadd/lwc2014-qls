package gui.view;

import lang.ast.Field;
import lang.ast.StringValue;
import lang.ast.ModelValue;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

/**
 * User: joerg
 * Created: 07.03.15 08:32
 */
public class QlWidgetStringStandard extends QlWidget {

  private JTextField textField;

  public QlWidgetStringStandard(Field modelNode) {
    super(modelNode);

    String labelText = modelNode.getLabel();
    ModelValue value = modelNode.getModelValue();

    JLabel label = new JLabel();
    label.setText(labelText);
    label.setForeground(new Color(254,254,254));
//    label.setBorder(new EmptyBorder(0, 0, 2, 0));
    label.setBorder(null);
    this.add(label);

    textField = new JTextField();
    textField.setText("");
    textField.setBorder(BorderFactory.createCompoundBorder(
        null,
        BorderFactory.createEmptyBorder(2, 2, 2, 2)));
    this.add(textField);

    this.setValue(value);

    GridLayout gridlayout = new GridLayout(2, 1);
    gridlayout.setVgap(0);
    gridlayout.setHgap(0);

    this.setLayout(gridlayout);
    this.setOpaque(false);
    this.setAlignmentX(Component.LEFT_ALIGNMENT);
    this.setBorder(new EmptyBorder(0, 0, 10, 0));
    this.setVisible(false);

    // Propagate input if focus is lost
    textField.addFocusListener(new FocusAdapter() {
      @Override
      public void focusGained(FocusEvent e) {
        textField.setBackground(Color.YELLOW.darker());
      }
      @Override
      public void focusLost(FocusEvent e) {
        textField.setBackground(Color.WHITE);
        updateModelNode(new StringValue(textField.getText()));
      }
    });

    textField.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateModelNode(new StringValue(textField.getText()));
      }
    });
  }

  @Override
  public void setValue(ModelValue value) {
    textField.setText(value.toString());
  }
}
