package gui.view;

import lang.ast.DateValue;
import lang.ast.Field;
import lang.ast.ModelValue;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * User: joerg
 * Created: 07.03.15 08:32
 */
public class QlWidgetDateStandard extends QlWidget implements PropertyChangeListener {

  private JFormattedTextField formattedTextField;
  private DateFormat dateFormat = new SimpleDateFormat(DateValue.getViewValueFormat());

  public QlWidgetDateStandard(Field modelNode) {
    super(modelNode);

    String labelText = modelNode.getLabel();
    ModelValue value = modelNode.getModelValue();

    JLabel label = new JLabel();
    label.setForeground(new Color(254,254,254));
    label.setText(labelText + " (" + DateValue.getViewValueFormat() + ")");
    this.add(label);

    formattedTextField = new JFormattedTextField(dateFormat);
    formattedTextField.setText("");
    formattedTextField.addPropertyChangeListener("value", this);
    formattedTextField.setBorder(BorderFactory.createCompoundBorder(
        null,
        BorderFactory.createEmptyBorder(2, 2, 2, 2)));

    try {
      MaskFormatter dateMask = new MaskFormatter(DateValue.getViewValueMaskFormat());
      dateMask.install(formattedTextField);
    } catch (ParseException ex) {
      formattedTextField.setText("");
    }

    this.add(formattedTextField);

    this.setValue(value);

    GridLayout gridlayout = new GridLayout(2, 1);
    gridlayout.setVgap(0);
    gridlayout.setHgap(0);

    this.setLayout(gridlayout);
    this.setOpaque(false);
    this.setAlignmentX(Component.LEFT_ALIGNMENT);
    this.setBorder(new EmptyBorder(0, 0, 10, 0));
    this.setVisible(false);

    // Propagate input if focus is lost
    formattedTextField.addFocusListener(new FocusAdapter() {
      @Override
      public void focusGained(FocusEvent e) {
        formattedTextField.setBackground(Color.YELLOW.darker());
      }

      @Override
      public void focusLost(FocusEvent e) {
        formattedTextField.setBackground(Color.WHITE);
      }
    });
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    JFormattedTextField formattedTextField = (JFormattedTextField) evt.getSource();
    Object formattedTextFieldValue = formattedTextField.getValue();
    if(isDate(formattedTextFieldValue)) {
      formattedTextField.setValue(formattedTextFieldValue);
      updateModelNode(new DateValue((Date)formattedTextFieldValue));
    }
  }

  @Override
  public void setValue(ModelValue value) {
    if (value.isDateValue()) {
      DateValue dateValue = (DateValue) value;
      formattedTextField.setText(dateValue.toString());
    } else {
      formattedTextField.setText(ModelValue.getUndefinedValue().toString());
    }
  }
  private boolean isDate(Object object) {
    return (object != null && object instanceof Date);
  }
    
}
