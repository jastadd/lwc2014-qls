package gui.view;

import gui.controller.ApplicationRunner;
import lang.ast.Problem;

import javax.swing.*;
import javax.swing.text.*;
import java.awt.*;
import java.util.Collection;

@SuppressWarnings("serial")
public class QlMessageBox extends JDialog {

  private JTextPane messages = new JTextPane();
  private DefaultStyledDocument document = new DefaultStyledDocument();
  private Style errorStyle;
  private Style warningStyle;

	public QlMessageBox() {
    
    StyleContext context = new StyleContext();
    errorStyle = context.addStyle("error", null);
    StyleConstants.setForeground(errorStyle, Color.RED);
    warningStyle = context.addStyle("warning", null);
    StyleConstants.setForeground(warningStyle, Color.BLUE);

    messages.setDocument(document);
		messages.setText("");
		messages.setEditable(false);

    JScrollPane qlScrollpane = new JScrollPane();
    qlScrollpane.setViewportView(messages);
    qlScrollpane.setPreferredSize(this.getMinimumSize());
    qlScrollpane.setAutoscrolls(true);
    qlScrollpane.getVerticalScrollBar().setUnitIncrement(20);
    qlScrollpane.setBorder(null);

    this.add(qlScrollpane);
		this.setTitle("Runtime warnings and errors");
    this.setResizable(true);

    QlWindow qlWindow = ApplicationRunner.getInstance(QlWindow.class);
    this.setMinimumSize(new Dimension((int)(qlWindow.getWidth() * 0.8), qlWindow.getHeight()));
    this.setLocation(qlWindow.getX() + qlWindow.getWidth() + 5, qlWindow.getY());

	}
  
  public void clear() {
    try {
      document.remove(0, document.getLength());
    } catch (BadLocationException e) {
      e.printStackTrace();
    }
  }

  public void addError(Problem problem) {
    try {
      String problemMessage = problem.toString().trim();
      if(!problemMessage.isEmpty()) {
        document.insertString(document.getLength(), problemMessage + "\n", errorStyle);
      }
    } catch (BadLocationException e) {
      e.printStackTrace();
    }
  }

  public void addWarning(Problem problem) {
    try {
      String problemMessage = problem.toString().trim();
      if(!problemMessage.isEmpty()) {
        document.insertString(document.getLength(), problemMessage + "\n", warningStyle);
      }
    } catch (BadLocationException e) {
      e.printStackTrace();
    }
  }
  
  public void addErrors(Collection<Problem> problems) {
    for(Problem problem : problems) {
      addError(problem);
    }
  }
  
  public void addWarnings(Collection<Problem> problems) {
    for(Problem problem : problems) {
      addWarning(problem);
    }
  }

}
