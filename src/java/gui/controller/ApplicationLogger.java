package gui.controller;

import java.io.IOException;
import java.util.logging.*;

public class ApplicationLogger {

	private static final String FILEHANDLER_FILE_NAME = "logs/log%g.txt";
	private static final ApplicationLoggerFormatter applicationLoggerFormatter = new ApplicationLoggerFormatter();

	public static Logger getFileLogger(String loggerName) {
		Logger applicationLogger;
		try {
			applicationLogger = Logger.getLogger(loggerName);
			FileHandler fileHandler = new FileHandler(FILEHANDLER_FILE_NAME);
			fileHandler.setFormatter(applicationLoggerFormatter);
			// We're using our own formatter
			applicationLogger.setUseParentHandlers(false);
			applicationLogger.addHandler(fileHandler);
			applicationLogger.setLevel(Level.INFO);

		} catch (SecurityException e) {
			throw new ApplicationException(ApplicationLogger.class.getName(), e);
		} catch (IOException e) {
			throw new ApplicationException(ApplicationLogger.class.getName(), e);
		}
		return applicationLogger;
	}

	public static Logger getConsoleLogger(String loggerName) {
		Logger applicationLogger;
		applicationLogger = Logger.getLogger(loggerName);
		ConsoleHandler consoleHandler = new ConsoleHandler();
		consoleHandler.setFormatter(applicationLoggerFormatter);
		applicationLogger.addHandler(consoleHandler);
		applicationLogger.setLevel(Level.INFO);
		return applicationLogger;
	}

	private static class ApplicationLoggerFormatter extends SimpleFormatter {

		@Override
		public String format(LogRecord logRecord) {
			String extendedSimpleFormat = "#############################################\n"
					+ super.format(logRecord);

			return extendedSimpleFormat;
		}

	}
}
