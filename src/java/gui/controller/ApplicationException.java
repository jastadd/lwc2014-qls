package gui.controller;

import javax.swing.*;
import java.util.logging.Level;
import java.util.logging.Logger;
@SuppressWarnings("serial")
public class ApplicationException extends RuntimeException {

	public enum Mode {
		LOG_ONLY, EXIT_GRACEFUL
	}

	public ApplicationException(Throwable originalException) {
		this(Mode.LOG_ONLY, originalException);
	}

	public ApplicationException(Mode mode, Throwable originalException) {

		super(originalException.getClass().getName(), originalException);

		Logger ppApplicationLogger = ApplicationLogger.getFileLogger(originalException.getClass().getName());
		ppApplicationLogger.log(Level.SEVERE, this.getMessage(),
				originalException);

		if (mode == Mode.EXIT_GRACEFUL) {
			JOptionPane.showMessageDialog(
              null,
              "Die Anwendung wird wegen eines schweren Fehlers beendet. Weitere Informationen können der Log-Datei entnommen werden.");
			System.exit(0);
		}
	}

	public ApplicationException(String message, Throwable originalException) {
		super(message, originalException);
		Logger ppApplicationLogger = ApplicationLogger.getFileLogger(ApplicationException.class.getName());
		ppApplicationLogger.log(Level.SEVERE, this.getMessage(),
				originalException);
	}
}
