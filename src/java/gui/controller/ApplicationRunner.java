package gui.controller;

import gui.view.QlMessageBox;
import gui.view.QlWindow;
import lang.ast.*;

import javax.swing.*;
import java.io.File;
import java.io.FileReader;
import java.util.Collection;
import java.util.HashMap;

public class ApplicationRunner {

  private static HashMap<Class<?>, Object> instances = new HashMap<Class<?>, Object>();

  private static Program program = null;
  private static boolean showTreeDump = false;
  private static boolean showWarnings = false;

	public static void main(String[] args) {
    try {
      if (args.length == 0) {
        System.err.println("You must specify a QL-Form file on the command line!");
        printUsage();
        System.exit(1);
      }
      String fileName = args[0];
      if(!new File(fileName).exists()) {
        System.out.println("File '" + fileName + "' not found!");
        System.exit(1);
      }

      for(String arg : args) {
        if( arg.equals("-d")) {
          showTreeDump = true;
        } else if(arg.equals("-w")) {
          showWarnings = true;
        }
      }

      LangScanner scanner = new LangScanner(new FileReader(fileName));
      LangParser parser = new LangParser();
      program = (Program) parser.parse(scanner);
      program.fileName = fileName;
      if(program.hasStaticErrors()) {
        showStaticErrors();
        dumpTree();
        System.exit(0);
      } else {
        System.out.println("All checks passed!");
      }

      // Setting L&F should be done as the very first step in the
      // application. Otherwise there's the risk of initializing the Java
      // L&F regardless of what L&F is requested
      setLookAndFeel();
      // Schedule a job for the event dispatch thread: creating and
      // showing this application's GUI

      SwingUtilities.invokeLater(new Runnable() {
        public void run() {
          getInstance(QlWindow.class);
        }
      });
    } catch (Exception e) {
      e.printStackTrace();
    }
	}

  public static void showRuntimeProblems() {
    boolean showRuntimeErrors = program.hasRuntimeErrors();
    boolean showRuntimeWarnings = showWarnings && program.hasRuntimeWarnings();
    if(showRuntimeErrors || showRuntimeWarnings) {
      QlMessageBox qlMessageBox = ApplicationRunner.getInstance(QlMessageBox.class);
      qlMessageBox.clear();
      if (showRuntimeErrors) {
        Collection<Problem> problems = program.getRuntimeErrors();
        qlMessageBox.addErrors(problems);
      }
      if (showRuntimeWarnings) {
        Collection<Problem> problems = program.getRuntimeWarnings();
        qlMessageBox.addWarnings(problems);
      }
      program.clearRuntimeProblems();
      qlMessageBox.setVisible(true);
    }
  }

  public static void showStaticErrors() {
    if (program.hasStaticErrors()) {
      Collection<Problem> problems = program.getStaticErrors();
      for(Problem problem : problems) {
        System.err.println(problem.toString());
      }
    }
  }
  
  public static void dumpTree() {
    if(showTreeDump) {
      System.out.println(program.dumpTree());
    }
  }

  public static void updateModel() {
    program.eval();
  }

  public static void updateView() {
    program.updateView();
    QlWindow qlWindow = ApplicationRunner.getExistingInstance(QlWindow.class);
    if(qlWindow != null) {
      qlWindow.updateView();
    }
//    JPanel qlPanel = ApplicationRunner.getInstance(JPanel.class);
//    Rectangle visible = qlPanel.getVisibleRect();
//    Rectangle bounds = qlPanel.getBounds();
//    visible.y = bounds.height;
//    qlPanel.scrollRectToVisible(visible);
  }

  public static Program getProgram() {
    return program;
  }

  @SuppressWarnings("unchecked")
  public final static <T> T getInstance(Class<T> singletonClass) {
    T result = (T) instances.get(singletonClass);
    if (result == null) {
      try {
        result = singletonClass.newInstance();
      } catch (InstantiationException e) {
        throw new ApplicationException(
            ApplicationException.Mode.EXIT_GRACEFUL, e);
      } catch (IllegalAccessException e) {
        throw new ApplicationException(
            ApplicationException.Mode.EXIT_GRACEFUL, e);
      }
      instances.put(singletonClass, result);
    }
    return result;
  }
  public final static <T> T getExistingInstance(Class<T> singletonClass) {
    return (T) instances.get(singletonClass);
  }

  /**
	 * Do some OS related cosmetics.
	 * 
	 * @see javax.swing.UIManager#setLookAndFeel(String)
	 */
	private static void setLookAndFeel() throws UnsupportedLookAndFeelException {
		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
		// Turn off metal's use of bold fonts
		UIManager.put("swing.boldMetal", Boolean.FALSE);
	}

  private static void printUsage() {
    System.err.println("     $ cd your/path/to/jastaddql");
    System.err.println("JAR: $ java -jar jastadd-ql.jar [filename] [options]");
    System.err.println("NIX: $ java -cp .:ant-bin/ gui.controller.ApplicationRunner [filename] [options]");
    System.err.println("WIN: $ java -cp \".;ant-bin/\" gui.controller.ApplicationRunner [filename] [options]");
    System.err.println("IDE: From a custom run configuration");
    System.err.println("Arguments: ");
    System.err.println("[filename]: The file that specifies the Ql-Form, e.g. testfiles/interface/BigBinary.ql");
    System.err.println("      [-d]: Optional argument to display debugging info");
    System.err.println("      [-w]: Optional argument to display warnings");

 	}
  
}
