package lang;

import lang.ast.LangParser;
import lang.ast.LangScanner;
import lang.ast.Problem;
import lang.ast.Program;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.TreeSet;

/**
 * Dumps the parsed Abstract Syntax Tree.
 */
public class Checker {
	/**
	 * Entry point
	 * @param args
	 */
	public static void main(String[] args) {
      
    try {
      if (args.length != 1) {
        System.err.println(
						"You must specify a source file on the command line!");
        printUsage();
        System.exit(1);
        return;
      }

      String filename = args[0];
      LangScanner scanner = new LangScanner(new FileReader(filename));
			LangParser parser = new LangParser();
      
      Program program = (Program) parser.parse(scanner);
      
//      program.collectErrors();
      if(program.hasStaticErrors()) {
        TreeSet<Problem> problems = program.getStaticErrors();
        for( Problem error : problems) {
          System.err.println(error);
        }
      } else {
        System.out.println("All checks passed!");
      }
		} catch (FileNotFoundException e) {
			System.out.println("File not found!");
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace(System.err);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void printUsage() {
		System.err.println("Usage: Checker FILE");
		System.err.println("  where FILE is the file to be parsed");
	}
}

