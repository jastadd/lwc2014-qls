import java.util.*;

aspect NameAnalysis {

	inh Program ASTNode.program();
	eq Program.getChild().program() = this;

  inh ASTNode Component.enclosingComponent();
  eq Group.getChild().enclosingComponent() = this;
  eq Form.getChild().enclosingComponent() = this;

  inh Component VarUse.usingComponent();
  eq ComputedField.getChild().usingComponent() = this;
  eq Group.getChild().usingComponent() = this;

  coll Set<VarDecl> Program.varDecls()
      [new HashSet<VarDecl>()]
      with add
      root Program;

  Field contributes getVarDecl() to Program.varDecls() for program();


  // Version 1: Check declare before use statically by using collection-attribute Program.varDecls() and lineNumber()
  // Error check condition: decl().isUnknown() == true
  //  syn VarDecl VarUse.decl() {
  //    for(VarDecl varDecl : program().varDecls()) {
  //     if(this.getName().equals(varDecl.getName()) &&
  //          varDecl.lineNumber() < this.lineNumber()) {
  //        return varDecl;
  //      }
  //    }
  //    return unknownDecl();
  //  }

  // Version 2: Check declare before use statically by perfoming upwards and downwards lookups
  // Error check condition: decl().isUnknown() == true
  syn VarDecl VarUse.decl() = lookup(getName());

  inh lazy VarDecl VarUse.lookup(String name);
  inh lazy VarDecl ComponentBlock.lookup(String name);
  eq ComponentBlock.getComponent(int index).lookup(String name) {
    for (int i = 0; i <= index; i++) {
      Component component = getComponent(i);
      VarDecl varDecl = component.localLookup(name);
      if(varDecl.isKnown()) {
        return varDecl;
      }
    }
    return this.lookup(name);
  }
  eq Program.getChild().lookup(String name) = unknownDecl();

  syn lazy VarDecl Component.localLookup(String name) = unknownDecl();
  eq ComponentBlock.localLookup(String name) {
    for( Component component : getComponents() ) {
      VarDecl varDecl = component.localLookup(name);
      if(varDecl.isKnown()) {
        return varDecl;
      }
    }
    return unknownDecl();
  }
  eq Group.localLookup(String name) {
    VarDecl varDecl = getThen().localLookup(name);
    if(varDecl.isUnknown() && hasElse()) {
      varDecl = getElse().localLookup(name);
    }
    return varDecl;
  }
  eq Field.localLookup(String name) = getVarDecl().getName().equals(name) ? getVarDecl() : unknownDecl();

	// Multiple identical declared fields inside a ComponentBlock
	// (which is a scope for visibility) are not allowed
	inh lazy VarDecl VarDecl.scopedLookup(String name);
	eq ComponentBlock.getChild().scopedLookup(String name) {
    for(Component component : getComponents()) {
      VarDecl varDecl = component.scopedLookup(name);
      if(varDecl.isKnown()) {
        return varDecl;
      }
    }
    return unknownDecl();
	}
	syn lazy VarDecl Component.scopedLookup(String name) = unknownDecl();
	eq Field.scopedLookup(String name) = localLookup(name);

  syn boolean VarDecl.isMultiplyDeclared() = scopedLookup(getName()) != this;

  syn boolean VarUse.isSelfReference() {
    if(getParent() instanceof ComputedField) {
      return ((ComputedField)getParent()).localLookup(getName()) .isKnown();
    }
    return false;
  }

}
